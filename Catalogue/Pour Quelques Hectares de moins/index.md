---
Edition: Editions REPAS
DépotLégal: 2011
Auteur: Christophe Beau
Titre: Pour Quelques Hectares de Moins
SousTitre: Tribulations coopératives d'un vigneron nomade 
Collection: Collection Pratiques Utopiques
ISBN :  978-2-919272-04-4
Pages: 150 pages
Prix: 16
Etat: Disponible
Résumé: |
    Momo, Bogus, Romuald, Cécile, Edgar, Birdee, Ricardo... sont quelques-uns des personnages chatoyants comme des vignes d’automne, de ce récit tout en péripéties. Ils participent à une aventure vigneronne collective qui recherche des voies autres pour vivre la vigne et le vin en liberté.

    Vous serez entraînés dans des aventures autant villageoises qu’aux quatre coins du monde, qui explorent d’autres manières de tutoyer cette belle liane et ce stimulant breuvage que sont la vigne et le vin. Mais il s'agit aussi d’envisager une viticulture vraiment paysanne, en « croît sens » plutôt qu’en croissance, et un commerce plus juste et partenarial.
Tags:
- Collectif
- Vin
- Alternatives
- Voyage
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/pour-quelques-hectares-de-moins-christophe-beau?_ga=2.142734133.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272044-pour-quelques-hectares-de-moins-tribulations-cooperatives-d-un-vigneron-nomade-christophe-beau/
SiteWeb: https://www.bogusateliers.wordpress.com
Couverture:
Couleurs:
  Fond: ' #8D409E'
  Titre: '#99E8F5'
  Texte: '#fff6d7'
  PageTitres: '#7d398c'
Adresse: 30260 Corconne
Latitude: 43.866978
Longitude: 3.933248
---

## L'auteur

**Christophe Beau** est vigneron en Languedoc, aussi ancré à son terroir qu'il est nomade, comme on le verra dans ce livre. Il a raconté dans un précédent ouvrage publié en 2003 aux éditions REPAS, La Danse des ceps, l'histoire de son « installation atypique », toute cousue de partenariats et de démarches aussi justes qu'échevelées. Cette fois-ci, il nous fait vivre ses tribulations autour du monde, que ce soit auprès des pionniers du renouveau vigneron français que des paysans du Chili ou du Minnesota, en passant par la Thaïlande et bien d'autres horizons.

## Extrait

> La vigne, elle aussi, méritait des égards. Il fallait la remettre en selle après tant d'années d'herbicides et d'engrais chimiques, qui lui profitaient certes, mais moi, je ne savais pas faire autrement qu'en bio !
>
> Avec ces souches quasi centenaires, plantées serrées à l'ancienne, inutile d'imaginer un labour au chenillard étroit que je n'avais pas. C'est avec Biscotte que je m'en sortis ! Je ne sais plus où j'avais rencontré Mark, le patron de Biscotte. Peut-être une petite annonce dans un ces petits journaux de la campagne alternative. J'avais fait sonner un portable et cela coupait sans cesse. Il roulait avec son semi-remorque sur l'autoroute du Sud. « Oui, la semaine prochaine, je serai proche ; dans les côtes du Rhône. Je ferai un saut chez toi pour une journée ou deux. Je te rappelle. »  Nouveau métier que celui de Mark, laboureur itinérant à cheval, qui consiste selon les saisons à sautiller de coteaux septentrionaux aux versants méridionaux, et inversement : pour sarcler, désherber, décavaillonner les rangs de vignes sans tasser les sols, rasant les vieux ceps sans les bousculer.
> -- page 11

## Le commentaire des éditeurs

Au-delà d'exemplarités sur de nouveaux modes de propriété ou de liens aux consommateurs, de pratiques biodynamiques ou de formes d'économie des ressources, ce livre nous invite à réfléchir sur les solutions mises en place à échelle humaine pour générer une économie et des filières que l'auteur identifie comme « associantes ».

## Du même auteur...

* *La Danse des Ceps. Chronique de vignes en partage*, Christophe Beau, 2009, Editions REPAS. [Voir la fiche du livre](/catalogue/danse-des-ceps/)
